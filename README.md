This task will be something quite simple and quick, just to have an overview of your set of skills and eye for detail.

- Create a form with 2 steps which should be:
    - Card Code (input box) and submit button
    - User information (First Name, Last Name, E-mail) and submit button
- Create an array of messages for success or error that should have: Title and description.
- If the Card code is an alphanumerical string with 13 characters, the second step should be shown, if not, a random validation (error) message should be shown.
- When the user clicks submit on the second step, a succesfull message should be shown.
- All assets needed are in "design" folder. Final designs are in "visual" folder.
- Mobile design is up to you, we usually work with 2 breakpoints in most of the cases which are < 767px and > 768px.
- Menues don't have to be like on live foreo.com - show/hide effect with submenues.

All details that are not defined in this README file means that author can decide on his/her own.
